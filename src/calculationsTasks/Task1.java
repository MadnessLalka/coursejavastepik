package calculationsTasks;

import java.util.Scanner;

public class Task1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int countOfFriends = sc.nextInt();
		int priceTeaOfKg = sc.nextInt();
		int priceOfOnePie = sc.nextInt();
		int countOfPie = sc.nextInt();
		
		int priceAllParty = ((priceTeaOfKg  * (countOfFriends  * 10) ) / 1000) + ((priceOfOnePie * countOfPie) * countOfFriends);
		
		System.out.println(priceAllParty);
		
		sc.close();
	}
}