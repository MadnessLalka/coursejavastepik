package calculationsTasks;

import java.util.Scanner;

public class Task10 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int countHorseLeg= sc.nextInt() * 4;
		int countGooseLeg = sc.nextInt() * 2;
		
		System.out.println(countHorseLeg + countGooseLeg);
		
		sc.close();
	}
}
