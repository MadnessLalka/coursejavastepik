package calculationsTasks;

import java.util.Scanner;

public class Task4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int countOfPage = sc.nextInt();
		int countOfPageEachDay = sc.nextInt();
		int countDay = sc.nextInt();
		
		System.out.println(countOfPage - (countOfPageEachDay * countDay));
		
		sc.close();
	}

}
