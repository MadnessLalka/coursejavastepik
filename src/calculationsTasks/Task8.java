package calculationsTasks;

import java.util.Scanner;

public class Task8 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		int d = sc.nextInt();
		
		System.out.println(n * d);
		
		sc.close();
	}
}
