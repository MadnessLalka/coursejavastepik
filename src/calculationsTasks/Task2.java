package calculationsTasks;

import java.util.Scanner;

public class Task2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int hour = sc.nextInt();
		int minute = sc.nextInt();
		
		System.out.println((hour * 60) + (minute));
		
		sc.close();
	}
}
