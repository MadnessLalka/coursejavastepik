package ArrayTasks;

import java.util.Scanner;

public class Task10 {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		int countSkittles = scanner.nextInt();
		int countSkittlesPush = scanner.nextInt();
		String[] arraySkittles = new String[countSkittles];

		for (int i = 0; i < countSkittles; i++) {
			arraySkittles[i] = "I";
		}

		for (int i = 0; i < countSkittlesPush; i++) {
			int startPush = scanner.nextInt();
			int finishPush = scanner.nextInt();

			for (int j = 0; j < countSkittles; j++) {
				if (j >= startPush - 1 && j <= finishPush - 1) {
					arraySkittles[j] = ".";
				}
			}
		}

		for (int i = 0; i < countSkittles; i++) {
			System.out.print(arraySkittles[i]);
		}
		scanner.close();
	}
}
//10 3 8 10 2 5 3 6