package ArrayTasks;

import java.util.Scanner;

public class Task16 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int lenghtArray = scanner.nextInt();
		int[] array = new int[lenghtArray];

		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}

		int countPairNextNumbers = scanner.nextInt();

		for (int i = 0; i < countPairNextNumbers; i++) {
			int indexFirstElement = scanner.nextInt();
			int indexSecondElement = scanner.nextInt();
			boolean flagFirstIndex = false;
			boolean flagSecondIndex = false;
			for (int j = 0; j < array.length; j++) {
				if (indexFirstElement == j) {
					flagFirstIndex = true;
				}
				if (indexSecondElement == j) {
					flagSecondIndex = true;
				}
			}
			if (flagFirstIndex == true && flagSecondIndex == true) {
				System.out.println(array[indexFirstElement] + array[indexSecondElement]);
			} else if (flagFirstIndex == false || flagSecondIndex == false) {
				System.out.println("Error");
			}
		}

		scanner.close();
	}
}
//5 12 3 -4 6 2 3 0 3 1 2 3 7