package ArrayTasks;

import java.util.Scanner;

public class Task8 {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		int lenghtArray = scanner.nextInt();
		int[] array = new int[lenghtArray];

		Boolean flagEvenNumbers = true;

		if (lenghtArray % 2 != 0) {
			flagEvenNumbers = false;
		}

		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}

		if (flagEvenNumbers == false) {
			for (int i = 0; i < array.length - 1; i++) {
				for (int j = i + 1; j < array.length - 1; j++) {
					int switcherNumber = array[j];
					if (array[i] <= array[j]) {
						array[j] = array[i];
						array[i] = switcherNumber;
					}
				}
				System.out.println(array[i]);
			}
			System.out.println(array[array.length - 1]);
		} else if (flagEvenNumbers == true) {
			for (int i = 0; i < array.length; i++) {
				for (int j = i + 1; j < array.length; j++) {
					int switcherNumber = array[j];
					if (array[i] <= array[j]) {
						array[j] = array[i];
						array[i] = switcherNumber;
					}
				}
				System.out.println(array[i]);
			}
		}
		scanner.close();
	}
}

// 6 4 5 3 4 2 3
// 5 4 5 3 4 0
// 5 4 5 3 4 10
// 1 2
// 3 -1 2 10