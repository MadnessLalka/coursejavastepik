package ArrayTasks;

import java.util.Scanner;

public class Task1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int[] arrayNumbers = new int[1001];

		for (int i = 0; i < arrayNumbers.length; i++) {
			arrayNumbers[i] = (int) Math.pow(i, 3);
		}

		int start = scanner.nextInt();
		int end = scanner.nextInt();

		if (start < end && end < arrayNumbers.length) {
			System.out.println(arrayNumbers[start]);
			System.out.println(arrayNumbers[end]);

		}

		scanner.close();
	}
}