package ArrayTasks;

import java.util.Scanner;

public class Task22 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int arrayLenght = scanner.nextInt();
		int[] array = new int[arrayLenght];

		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}

		int countNumbers = scanner.nextInt();
		if (countNumbers > 0 && countNumbers <= arrayLenght) {
			int counter = 0;
			for (int i = 0; i <= countNumbers - 1; i++) {
				counter += array[i];
			}

			System.out.println(counter);
		}

		scanner.close();
	}
}
//5 1 2 3 5 4 5
//8 1 34 56 23 76 56 90 4 3