package ArrayTasks;

import java.util.Scanner;

public class Task20 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int arrayLenght = scanner.nextInt();
		int[] array = new int[arrayLenght];

		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}

		int findNumberByIndex = scanner.nextInt();
		boolean flagNumber = true;

		for (int i = 0; i < array.length; i++) {
			if (findNumberByIndex == array[i]) {
				System.out.println(i);
				flagNumber = true;
				break;
			} else {
				flagNumber = false;
			}
		}

		if (flagNumber == false) {
			System.out.println("Error");
		}

		scanner.close();
	}
}
//5 1 2 3 5 4 5
//4 5 3 2 0