package ArrayTasks;

import java.util.Scanner;

public class Task23 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int arrayLenght = scanner.nextInt();
		int[] array = new int[arrayLenght];

		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}

		int startNumbers = scanner.nextInt();
		int finishNumbers = scanner.nextInt();

		if (startNumbers >= 0 && startNumbers < finishNumbers && finishNumbers <= arrayLenght) {
			int counter = 0;
			int sumNumber = 0;
			for (int i = startNumbers; i <= finishNumbers; i++) {
				sumNumber += array[i];
				counter++;
			}
			System.out.println(sumNumber / (double)counter);
		}

		scanner.close();
	}
}
//5 1 2 3 5 4 5
//8 1 34 56 23 76 56 90 4 3