package ArrayTasks;

import java.util.Scanner;

public class Task15 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int arrayLenght = scanner.nextInt();
		int[] array = new int[arrayLenght];

		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}

		int firstNumber = scanner.nextInt();
		int secondNumber = scanner.nextInt();
		boolean flagFirstNumber = false;
		boolean flagSecondNumber = false;

		for (int i = 0; i < array.length; i++) {
			if (firstNumber == i) {
				flagFirstNumber = true;
			}
			if (secondNumber == i) {
				flagSecondNumber = true;
			}
		}

		if (flagFirstNumber == true && flagSecondNumber == true) {
			System.out.println(array[firstNumber] + array[secondNumber]);
		} else if (flagFirstNumber == false || flagSecondNumber == false) {
			System.out.println("Error");
		}

		scanner.close();
	}
}
//5 12 3 -4 6 2 0 3