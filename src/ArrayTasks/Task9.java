package ArrayTasks;

import java.util.Scanner;

public class Task9 {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		int start = scanner.nextInt();
		int finish = scanner.nextInt();
		int lenghtCubeArray = finish - start;
		int countNumber = scanner.nextInt();
		
		if (start < finish) {
			for (int i = 0; i < countNumber; i++) {
				int number = scanner.nextInt();
				boolean flagEqual = true;
				for (int z = start; z <= finish; z++) {
					if (number == z) {
						flagEqual = true;
						break;
					} else if (number != z) {
						flagEqual = false;
					}
				}

				if (flagEqual == true) {
					System.out.println((int) Math.pow(number, 3));
				} else if (flagEqual == false) {
					System.out.println("Error");
				}
			} 
		}
		scanner.close();
	}
}
// 5 10 4 5 8 -1 5
// 0 5 4 5 8 0 5