package CycleTasks;

import java.util.Scanner;

public class Task26 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int firstNumber = scanner.nextInt();
		int secondNumber = scanner.nextInt();
		int sumNumbers = 0;

		for (int i = firstNumber; i <= secondNumber; i++) {
			sumNumbers += i;
		}
		System.out.println(sumNumbers);

		scanner.close();
	}
}