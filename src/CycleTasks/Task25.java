package CycleTasks;

import java.util.Scanner;

public class Task25 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int countNumbers = scanner.nextInt();
		int integerNumbers = 0;
		int sumNumbers = 0;
		int counter = 0;

		while (counter < countNumbers) {

			integerNumbers = scanner.nextInt();
			sumNumbers += integerNumbers;
			counter++;

		}
		System.out.println(sumNumbers);

		scanner.close();
	}
}