package CycleTasks;

import java.util.Scanner;

public class Task36 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int counterMax = 0;
		int maxNumber = 0;
		int number = 0;

		for (int i = 0; i <= 10000; i++) {
			number = scanner.nextInt();
			if (number == 0) {
				break;
			} else if (number > maxNumber) {
				counterMax = 1;
				maxNumber = number;
			} else if (maxNumber == number) {
				counterMax++;
			}
		}

		System.out.println(counterMax);

		scanner.close();
	}
}