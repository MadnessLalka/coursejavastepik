package CycleTasks;

import java.util.Scanner;

public class Task47 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		Boolean flag = false;
		int countNumbers = scanner.nextInt();

		for (int i = 0; i < countNumbers; i++) {
			int number = scanner.nextInt();
			if (number % 10 == 4 || number == 4) {
				flag = true;
			}
		}

		if (flag == true) {
			System.out.println("Yes");
		} else if (flag == false) {
			System.out.println("No");
		}

		scanner.close();
	}
}