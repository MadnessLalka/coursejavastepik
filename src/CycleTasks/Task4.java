package CycleTasks;

import java.util.Scanner;

public class Task4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int X = sc.nextInt();
		int Y = sc.nextInt();
		int count = 0;
		
		while (X <= Y) {
			if (X % 3 == 0 && X % 5 != 0) {
				count++;
			}
			X++;
		}
		System.out.println(count);
		
		sc.close();
	}
}