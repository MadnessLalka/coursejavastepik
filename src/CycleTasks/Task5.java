package CycleTasks;

import java.util.Scanner;

public class Task5 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double number = sc.nextDouble();
		int count = 0;
		double avgSum = number;
	
		if (number != 0) {
			while (number != 0) {
				 number = sc.nextDouble();
				 avgSum += number;
				 count ++; 
			}
			System.out.println(avgSum / count);
		}
		
		sc.close();
	}
}