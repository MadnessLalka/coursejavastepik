package CycleTasks;

import java.util.Scanner;

public class Task13 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int number = sc.nextInt();
		int count = 0;
		
		while (count < number) {
			System.out.println(count+1);
			count ++;
		}
		sc.close();
	}
}