package CycleTasks;

import java.util.Scanner;

public class Task30 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int counterNumbers = scanner.nextInt();
		int factorial = 1;

		for (int i = 1; i <= counterNumbers; i++) {
			factorial *= i;
		}
		System.out.println(factorial);
	}
}