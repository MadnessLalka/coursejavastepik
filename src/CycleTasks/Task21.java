package CycleTasks;

import java.util.Scanner;

public class Task21 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int numbersOfSteps = sc.nextInt();
		int counterSteps = 1;
		String dot = "*";
		int count = 1;
		String space = "";
		
			while (count <= (numbersOfSteps - 1)) {
				space += " ";
				count++;
			}
			System.out.println(space + dot);
	
		while (counterSteps < numbersOfSteps) {
			 count = 1;
			 space = "";

				while (count <= (numbersOfSteps - (counterSteps + 1))) {
					space += " ";
					count++;
				}
				System.out.println(space + (dot += "**"));
			counterSteps++;
		}
		sc.close();
	}
}