package CycleTasks;

import java.util.Scanner;

public class Task34 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int counter = 0;
		boolean flag = false;

		for (String words = scanner.next(); !words.equals("СТОП"); words = scanner.next()) {
			counter++;

			if (words.equals("ээээ") || words.equals("потом")) {
				counter--;
			}

			if (words.equals("Куб")) {
				flag = true;
				System.out.println(counter);
				break;
			}
		}

		if (flag == false) {
			System.out.println("NO");
		}
	}
}