package CycleTasks;

import java.util.Scanner;

public class Task2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int number = sc.nextInt();
		int numberCount = 1;
	
		
		while (Math.pow(numberCount, 2) <= number) {
			System.out.println((int)Math.pow(numberCount, 2) );
			numberCount ++;
		}
		sc.close();
	}
	
}