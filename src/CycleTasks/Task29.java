package CycleTasks;

import java.util.Scanner;

public class Task29 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int countNumber = scanner.nextInt();
		int integerNumber = scanner.nextInt();
		int maximumNumber = integerNumber;
		int minimumNumber = integerNumber;
		
		for (int i = 0; i < countNumber-1; i++) {
			integerNumber = scanner.nextInt();
			
			if (integerNumber > maximumNumber) {
				maximumNumber = integerNumber;
			}
			
			if (integerNumber < minimumNumber) {
				minimumNumber = integerNumber;
			}
		}
		System.out.println(minimumNumber);
		System.out.println(maximumNumber);
	}
}