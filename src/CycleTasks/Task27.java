package CycleTasks;

import java.util.Scanner;

public class Task27 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int integerNumber;
		int counter = 0;

		for (int i = scanner.nextInt(); i % 7 != 0; i = scanner.nextInt()) {

			if (i % 10 == 7) {
				counter += i;
			}

		}
		System.out.println(counter);

	}
}