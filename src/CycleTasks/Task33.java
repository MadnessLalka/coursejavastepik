package CycleTasks;

import java.util.Iterator;
import java.util.Scanner;

public class Task33 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int N = scanner.nextInt();
		int counter;
		Boolean flagFinish = false;
		
		while(N != 0) {
			System.out.print(N % 2);
			N /= 2;
		}
	}
}
