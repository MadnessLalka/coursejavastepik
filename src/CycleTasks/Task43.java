package CycleTasks;

import java.util.Scanner;

public class Task43 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int a = scanner.nextInt();
		int b = scanner.nextInt();
		int c = scanner.nextInt();

		for (int i = a; i >= b; i -= c) {
			System.out.println(i);
		}

		scanner.close();
	}
}