package CycleTasks;

import java.util.Scanner;

public class Task42 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		long a = scanner.nextLong();
		long b = scanner.nextLong();
		long counter = 1;

		while (a <= b) {
			counter *= a;
			a++;
		}
		System.out.println(counter);

		scanner.close();
	}
}