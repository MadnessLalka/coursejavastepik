package CycleTasks;

import java.util.Scanner;

public class Task3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int X = sc.nextInt();
		int Y = sc.nextInt();
		int count = 0;
		
		while (X <= Y) {
			count = count + X;
			X++;
		}
		System.out.println(count);
		
		sc.close();
	}
}