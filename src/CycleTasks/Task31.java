package CycleTasks;

import java.util.Scanner;

public class Task31 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		Boolean flag = false;
		int counter = 1;

		for (String i = scanner.next(); !i.equals("СТОП"); i = scanner.next()) {
			if (Integer.parseInt(i) != 0) {
				counter *= Integer.parseInt(i);
			}
		}

		if (counter == 1) {

			System.out.println("Не найден");

		} else {
			System.out.println(counter);
		}
	}
}