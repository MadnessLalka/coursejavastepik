package CycleTasks;

import java.util.Scanner;

public class Task24 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int countNumbers = scanner.nextInt();
		int counter = 0;
		int newNumbers;
		int countEvenNumbers = 0;

		while (counter < countNumbers) {
			newNumbers = scanner.nextInt();
			if (newNumbers % 2 == 0) {
				countEvenNumbers++;
			}

			counter++;
		}
		System.out.println(countEvenNumbers);

		scanner.close();
	}
}