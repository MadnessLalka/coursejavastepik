package CycleTasks;

import java.util.Scanner;

public class Task11 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int number = sc.nextInt();
		int modPart = 0;
		int divPart = number;
		
		modPart = number % 10;
		while (divPart / 10 != 0) {
			divPart /= 10;
			modPart += divPart % 10;
		}
		System.out.println(modPart);
		
		sc.close();
	}
}