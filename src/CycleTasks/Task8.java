package CycleTasks;

import java.util.Scanner;

public class Task8 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		double X = sc.nextDouble();
		int Y = sc.nextInt();
		int count = 0;
		
		while (X < Y) {
			X+=  X * 0.10;
			System.out.println(X);
			count++;
		}
		System.out.println(count);
		
		sc.close();
	}
}