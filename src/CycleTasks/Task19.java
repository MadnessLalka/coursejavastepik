package CycleTasks;

import java.util.Scanner;

public class Task19 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int number = sc.nextInt();
		int count = number;

		while (number != 0) {
			number = sc.nextInt();
			count += number;
		}
		System.out.println(count);

		sc.close();
	}
}