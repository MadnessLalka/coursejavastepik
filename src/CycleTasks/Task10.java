package CycleTasks;

import java.util.Scanner;

public class Task10 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int number = sc.nextInt();
		int count = number;
		int minNumber = 0;
		
		if (number >= 2) {
			while (count > 1) {
				if ( number % count == 0) {
					minNumber = count;
				}
				count --;
			}
			System.out.println(minNumber);
		}
		sc.close();
	}
}