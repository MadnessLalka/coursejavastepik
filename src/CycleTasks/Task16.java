package CycleTasks;

import java.util.Scanner;

public class Task16 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int startNumber = sc.nextInt();
		int endNumber = sc.nextInt();
		
		while (startNumber < endNumber || startNumber > endNumber || startNumber == endNumber) {
			if (startNumber < endNumber) {
				System.out.println(startNumber);
				startNumber ++;
			} else if (startNumber > endNumber ) {
				System.out.println(startNumber);
				startNumber --;
			} else if (startNumber == endNumber ) {
				System.out.println(startNumber);
				break;
			}
		}
		sc.close();
	}
}