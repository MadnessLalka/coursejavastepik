package CycleTasks;

import java.util.Scanner;

public class Task20 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int number = sc.nextInt();
		int xber = sc.nextInt();
		int counter = xber;

		while (xber < number) {
			xber += counter;
		}
		System.out.println(xber);

		sc.close();
	}
}