package CycleTasks;

import java.util.Scanner;

public class Task41 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int counter = 1;

		while (counter < 10) {
			if (counter % 2 != 0) {
				System.out.print(counter + " ");
			}

			counter++;
		}

		scanner.close();
	}
}