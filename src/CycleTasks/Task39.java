package CycleTasks;

import java.util.Scanner;

public class Task39 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int numberStart = scanner.nextInt();
		int numberFinish = scanner.nextInt();
		int sum = 0;

		for (int i = numberStart; i <= numberFinish; i++) {
			System.out.println(i + " " + i + ".$");
		}

		scanner.close();
	}
}