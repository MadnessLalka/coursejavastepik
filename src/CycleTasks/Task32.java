package CycleTasks;

import java.util.Scanner;

public class Task32 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int a = scanner.nextInt();
		int n = scanner.nextInt();
		int formula = 0;
		
		for (int i = 0; i <= n ;i++) {
			formula += Math.pow(a, i);
		}
		System.out.println(formula);
	}
}