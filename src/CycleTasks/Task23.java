package CycleTasks;

import java.util.Scanner;

public class Task23 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int counter = 0;
		int multiplicationNumbers = 1;
		int integerNumber = scanner.nextInt();
		int newNumbers;

		if (integerNumber == 1 || integerNumber == -1) {
			
			newNumbers = scanner.nextInt();
			multiplicationNumbers *= newNumbers;
			
			counter++;
			
			System.out.println(counter + " " + multiplicationNumbers);

		} else {
			while (multiplicationNumbers < integerNumber) {

				newNumbers = scanner.nextInt();

				multiplicationNumbers *= newNumbers;
				counter++;
			}
			System.out.println(counter + " " + multiplicationNumbers);
		}
		scanner.close();
	}
}