package CycleTasks;

import java.util.Scanner;

public class Task40 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		long countNumbers = scanner.nextLong();
		long multiplicationNumbers = 1;

		for (int i = 0; i < countNumbers; i++) {
			long number = scanner.nextLong();
			multiplicationNumbers *= number;
		}
		System.out.println(multiplicationNumbers);

		scanner.close();
	}
}