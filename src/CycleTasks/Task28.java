package CycleTasks;

import java.util.Scanner;

public class Task28 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		Boolean flag = false;

		for (String i = scanner.next(); !i.equals("СТОП"); i = scanner.next()) {
			if (i.equals("Куб")) {
				flag = true;
			}
		}
		if (flag == true) {
			System.out.println("YES");
		} else {
			System.out.println("NO");
		}
	}
}