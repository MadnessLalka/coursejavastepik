package CycleTasks;

import java.util.Scanner;

public class Task18 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int number = sc.nextInt();
		int count = 0;
		
		while (number != 0) {
			number = sc.nextInt();
			count ++;
		}
		System.out.println(count);
		
		sc.close();
	}
}