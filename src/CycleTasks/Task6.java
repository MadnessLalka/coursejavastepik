package CycleTasks;

import java.util.Scanner;

public class Task6 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int numbers1 = sc.nextInt();
		int numbers2 = sc.nextInt();
		int maxNumber = numbers1;
		
		if (numbers1 != 0 && numbers2 != 0) {
			while (numbers2 != 0) {
				numbers2 = sc.nextInt();
				if (maxNumber > numbers2) {
					continue;
				} else {
					maxNumber = numbers2;
				}
			}
			System.out.println(maxNumber);
		}
		sc.close();
	}
}