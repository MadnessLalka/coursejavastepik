package CycleTasks;

import java.util.Scanner;

public class Task15 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int startNumber = sc.nextInt();
		int endNumber = sc.nextInt();
		int count;
		
		while (startNumber <= endNumber) {
			System.out.println(startNumber);
			startNumber ++;
		}
		
		sc.close();
	}
}