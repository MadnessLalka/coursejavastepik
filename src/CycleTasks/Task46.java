package CycleTasks;

import java.util.Scanner;

public class Task46 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int naturalNumber = scanner.nextInt();
		int modNumber = 1;
		int sumNaturalNumbers = naturalNumber % 10;

		while (naturalNumber > 0) {
			
			naturalNumber = naturalNumber / 10;
			if (naturalNumber == 0) {
				break;
			}
			modNumber = naturalNumber % 10;
			
			sumNaturalNumbers *= modNumber;
		}

	System.out.println(sumNaturalNumbers);

		scanner.close();
	}
}