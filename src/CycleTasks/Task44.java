package CycleTasks;

import java.util.Scanner;

public class Task44 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int countChildren = scanner.nextInt();
		int averageChildrenAge = 0;

		for (int i = 0; i < countChildren; i++) {

			String firstName = scanner.next();
			int age = scanner.nextInt();

			averageChildrenAge += age;
		}
		System.out.println(averageChildrenAge / (double) countChildren);

		scanner.close();
	}
}