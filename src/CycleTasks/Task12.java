package CycleTasks;

import java.util.Scanner;

public class Task12 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int firstNumber = sc.nextInt();
		int secondNumber;
		int countMaxNumbers = 0;
		
		if (firstNumber > 0) {
			secondNumber = sc.nextInt();
			if (secondNumber > 0) {
				while (secondNumber != 0) {
					if (firstNumber < secondNumber) {
						countMaxNumbers ++;
						firstNumber = secondNumber;
					} else {
						firstNumber = secondNumber;
					}
					secondNumber = sc.nextInt();
				}
			System.out.println(countMaxNumbers);
			}
		}
		
		sc.close();
	}
}