package CycleTasks;

import java.util.Scanner;

public class Task14 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		/*2 3 4 5 8 7 4 0*/
		
		int firstNumber = sc.nextInt();
		int secondNumber;
		int maxNumber =firstNumber;
		int avgNumber = 0;
		
		if (firstNumber > 0) {
			secondNumber = sc.nextInt();
			if (secondNumber > 0) {
				while (secondNumber != 0 && secondNumber > 0) {
					 if (maxNumber < secondNumber ) {
						 avgNumber = maxNumber;
						maxNumber = secondNumber;
						System.out.println(avgNumber);
					} else if (maxNumber > secondNumber && avgNumber < maxNumber &&  avgNumber < secondNumber)
					{
						avgNumber = secondNumber;
					}
					secondNumber = sc.nextInt();
				}
				System.out.println(avgNumber);
			}
		}

		sc.close();
	}
}