package CycleTasks;

import java.util.Scanner;

public class Task45 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int numberOwls = scanner.nextInt();
		int count = 0;

		while (count < numberOwls) {
			System.out.println(":)\\_____/(:");
			System.out.println(" {(@)v(@)}");
			System.out.println(" {|~- -~|}");
			System.out.println(" {/^'^'^\\}");
			System.out.println(" ===m-m===");
			System.out.println();
			count++;
		}

		scanner.close();
	}
}