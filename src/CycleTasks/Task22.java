package CycleTasks;

import java.util.Scanner;

public class Task22 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int numbersOfSteps = sc.nextInt();
		int counterSteps = 0;
		String dot = "";

		while (counterSteps < numbersOfSteps) {
			System.out.println(dot += "*");
			counterSteps++;
		}

		sc.close();
	}
}