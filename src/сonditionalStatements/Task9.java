package сonditionalStatements;

import java.util.Scanner;

public class Task9 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		double temperature = sc.nextDouble();
		
		if (temperature < 22.4) {
			System.out.print("Холодно(");
		}  else if(temperature == 22.4) {
			System.out.print("Прохладно...");
		} else if (temperature > 22.4) {
			System.out.print("Тепло!");
		}
	
		sc.close();
	}
}