package сonditionalStatements;

import java.util.Scanner;

public class Task1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int countPie = sc.nextInt();
		if (countPie <= 10) {
			System.out.println(countPie / 2);
		} else if(countPie > 10) {
			int priceWolf = countPie / 2;
			if (priceWolf > (countPie - 10)) {
				System.out.println(priceWolf);
			}else{
				System.out.println(countPie - 10);
			}
		}
		sc.close();
	}
}
