package сonditionalStatements;

import java.util.Scanner;

public class Task6 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		String firstLine = sc.nextLine();
		String secondLine = sc.nextLine();
		String thirdLine = sc.nextLine();
		
		if ((firstLine.equals(secondLine) || firstLine.equals(thirdLine)) && (!secondLine.equals(thirdLine) || !thirdLine.equals(secondLine) )) {
			System.out.println("Yes");
		} else if ((secondLine.equals(firstLine) || secondLine.equals(thirdLine)) && (!firstLine.equals(thirdLine) || !thirdLine.equals(firstLine)))  {
			System.out.println("Yes");
		}else if ((thirdLine.equals(firstLine) || thirdLine.equals(secondLine)) && (!firstLine.equals(secondLine) || !secondLine.equals(firstLine))) { 
			System.out.println("Yes");
		} else {
			System.out.println("No");
		}
		
		sc.close();
	}
}