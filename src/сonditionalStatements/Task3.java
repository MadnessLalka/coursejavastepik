package сonditionalStatements;

import java.util.Scanner;

public class Task3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int firstNumber = sc.nextInt();
		int secondNumber = sc.nextInt();
		int thirdNumber = sc.nextInt();
		
		
		if (firstNumber == secondNumber) {
			if (firstNumber > thirdNumber) {
				System.out.println(firstNumber);
			} else {
				System.out.println(thirdNumber);
			} 
		}else if(secondNumber == thirdNumber) {
			if (secondNumber > firstNumber) {
				System.out.println(secondNumber);
			} else {
				System.out.println(firstNumber);
			} 
		} else {
			if((firstNumber > secondNumber) && (firstNumber > thirdNumber)) {
				System.out.println(firstNumber);
			} else if (((secondNumber > firstNumber) && (secondNumber > thirdNumber))) {
				System.out.println(secondNumber);
			}else if (((thirdNumber > firstNumber) && ( thirdNumber>secondNumber))) {
				System.out.println(thirdNumber);
			}
		}
		sc.close();
	}
}