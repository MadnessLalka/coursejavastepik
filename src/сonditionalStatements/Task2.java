package сonditionalStatements;

import java.util.Scanner;

public class Task2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int hour = sc.nextInt();
		int minute = sc.nextInt();
		int timeUp = sc.nextInt();
		int allTimeSecond = ((60 * 60) * hour ) + (minute * 60);
			
		if ( timeUp > allTimeSecond) {
			System.out.println("Опоздал");
		} else  if( timeUp <= allTimeSecond){
			System.out.println("Успел");
		}
		sc.close();
	}
}