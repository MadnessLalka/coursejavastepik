package сonditionalStatements;

import java.util.Scanner;

public class Task20 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		double X = sc.nextDouble();
		double Y = sc.nextDouble();
		
		if (Math.pow(Y, 2) + Math.pow(X, 2) < 5  && 0.5 * X - 1 < Y &&  (Math.pow(X, 2) -3) > Y || Math.pow(Y, 2) + Math.pow(X, 2) > 5  && 0.5 * X - 1 > Y &&  (Math.pow(X, 2) -3) < Y) {
			System.out.println("Yes");
		} else {
			System.out.println("No");
		}
		
		sc.close();
	}
}