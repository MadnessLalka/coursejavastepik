package сonditionalStatements;

import java.util.Scanner;

public class Task8 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int widthSwimmingPool = sc.nextInt();
		int heightSwimmingPool = sc.nextInt();
		int wayLongEdge = sc.nextInt();
		int wayShortEdge = sc.nextInt();
		
		if (wayLongEdge == wayShortEdge) {
			System.out.print(wayLongEdge);
		} else if (wayLongEdge < wayShortEdge) {
			System.out.print(wayLongEdge);
		} else {
			System.out.print(wayShortEdge);
		}
	
		sc.close();
	}
}