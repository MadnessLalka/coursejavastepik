package MatrixTasks;

import java.util.Scanner;

public class Task10 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int lenghtMatrix = scanner.nextInt();
		if (lenghtMatrix != 0) {

			int[][] matrix = new int[lenghtMatrix][lenghtMatrix];

			for (int i = 0; i < lenghtMatrix; i++) {
				for (int j = 0; j < lenghtMatrix; j++) {
					matrix[i][j] = scanner.nextInt();
					System.out.print(matrix[i][j] + " ");
				}
				System.out.println();
			}

			System.out.println();

			int switcherMoreEdge = 0;
			int switcherLessEdge = 0;

			for (int i = 0; i < lenghtMatrix; i++) {
				for (int j = 0; j < lenghtMatrix; j++) {
					if (i != j) {
						if (i < j) {
							switcherMoreEdge = matrix[i][j];
							matrix[i][j] = matrix[j][i];
							matrix[j][i] = switcherMoreEdge;
						} /*
							 * else if (i > j) { switcherLessEdge = matrix[j][i]; matrix[j][i] =
							 * switcherMoreEdge; }
							 */

					}
					System.out.print(matrix[i][j] + " ");
				}
				System.out.println();
			}

			scanner.close();
		} else {
			System.out.println(0);
		}
	}
}
// 4 1 2 3 8 4 6 7 8 2 6 3 4 5 9 3 0
