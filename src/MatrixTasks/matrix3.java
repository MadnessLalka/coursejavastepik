package Matrix;

import java.util.Scanner;

public class matrix3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countOfArray = scanner.nextInt();
        int[] array = new int[countOfArray];

        if (countOfArray >= 1 && countOfArray <= 100) {
            for (int i = 0; i < countOfArray; i++) {
                array[i] = scanner.nextInt();
            }

            for (int i = 0; i < countOfArray; i++) {
                if (array[i] % 2 == 0) {
                    System.out.println(array[i] + " ");
                }
            }
        }
    }
}