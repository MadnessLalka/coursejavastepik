package Matrix;

import java.util.Scanner;

public class matrix7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countOfArray = scanner.nextInt();

        if (countOfArray < 10) {
            int[][] array = new int[countOfArray][countOfArray];

            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array.length; j++) {
                    if (j == array.length - i - 1) {
                        array[i][j] = 1;
                    } else if (j > array.length - i - 1) {
                        array[i][j] = 2;
                    }
                    System.out.print(array[i][j] + " ");
                }
                System.out.println();
            }
        }
    }
}