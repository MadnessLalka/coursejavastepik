package Matrix;

import java.util.Scanner;

public class matrix2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countOfArray = scanner.nextInt();
        int[] array = new int[countOfArray];

        if (countOfArray >= 1 && countOfArray <= 100) {
            for (int i = 0; i < countOfArray; i++) {
                array[i] = scanner.nextInt();
            }

            for (int i = 0; i < countOfArray; i += 2) {
                System.out.print(array[i] + " ");

            }

        }
    }
}