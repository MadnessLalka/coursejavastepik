package Matrix;

import java.util.Scanner;

public class matrix4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countOfArray = scanner.nextInt();
        int[] array = new int[countOfArray];

        if (countOfArray >= 1 && countOfArray <= 10000) {
            for (int i = 0; i < countOfArray; i++) {
                array[i] = scanner.nextInt();
            }

            boolean isDoubleSign = false;

            for (int i = 0; i < array.length - 1; i++) {
                if ((array[i] > 0 && array[i + 1] > 0) || (array[i] < 0 && array[i + 1] < 0)) {
                    System.out.println("YES");
                    isDoubleSign = true;
                    break;
                }
            }

            if (isDoubleSign == false) {
                System.out.println("NO");
            }
        }
    }
}