package MatrixTasks;

import java.util.Scanner;

public class Task7 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int heightMatrix = scanner.nextInt();
		int widthMatrix = 0;

		if (heightMatrix > 0) {

			int[][] matrix = new int[heightMatrix][];
			int[] sumArray = new int[heightMatrix];

			for (int i = 0; i < heightMatrix; i++) {
				widthMatrix = scanner.nextInt();
				matrix[i] = new int[widthMatrix];
				for (int j = 0; j < widthMatrix; j++) {
					matrix[i][j] = scanner.nextInt();
					sumArray[i] += matrix[i][j];
				}
			}

			int maxSum = sumArray[0];
			int indexMaxSum = 0;
			for (int i = 0; i < sumArray.length; i++) {
				if (sumArray[i] > maxSum) {
					maxSum = sumArray[i];
					indexMaxSum = i;
				}
			}

			System.out.println(indexMaxSum + 1);
			System.out.println(maxSum);

			for (int i = 0; i < widthMatrix; i++) {
				System.out.print(matrix[indexMaxSum][i] + " ");
			}

		}

		scanner.close();
	}
}
// 4 3 1 8 9 5 1 -2 3 6 4 1 1 1 1 1 1 3 -1 2 5 
// 3 4
//3 5 6 7
//8 9 39 2
//1 9 3 1

//3 4
//1 5 6 7
//8 9 39 2
//5 9 3 4

//3 3
//1 5 6 
//8 1 39 
//5 9 3 