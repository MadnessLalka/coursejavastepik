package Matrix;

import java.util.Scanner;

public class matrix10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int lenghtOfRows = scanner.nextInt();
        int lenghtOfColumns = scanner.nextInt();

        int[][] matrixOfHammerThrowning = new int[lenghtOfRows][lenghtOfColumns];

        for (int i = 0; i < lenghtOfRows; i++) {
            for (int j = 0; j < lenghtOfColumns; j++) {
                matrixOfHammerThrowning[i][j] = scanner.nextInt();
            }
        }

        int maxElement = matrixOfHammerThrowning[0][0];
        int maxIndexIOfEleemnt = 0;
        int maxIndexJOfEleemnt = 0;
        for (int i = 0; i < lenghtOfRows; i++) {
            for (int j = 0; j < lenghtOfColumns; j++) {
                if (maxElement < matrixOfHammerThrowning[i][j]) {
                    maxElement = matrixOfHammerThrowning[i][j];
                    maxIndexIOfEleemnt = i;
                    maxIndexJOfEleemnt = j;
                }
            }
        }

        System.out.println(maxElement);
        System.out.print(maxIndexIOfEleemnt + " " + maxIndexJOfEleemnt);

    }
}