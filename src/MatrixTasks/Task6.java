package MatrixTasks;

import java.util.Scanner;

public class Task6 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int heightMatrix = scanner.nextInt();
		int widthMatrix = scanner.nextInt();

		if (heightMatrix > 0 || widthMatrix > 0) {

			int[][] matrix = new int[heightMatrix][widthMatrix];

			int indexeMinimalI = 0;
			int indexeMinimalJ = 0;
			boolean flagTrueMinimalism = true;

			for (int i = 0; i < heightMatrix; i++) {
				for (int j = 0; j < widthMatrix; j++) {
					matrix[i][j] = scanner.nextInt();
				}
			}

			int minimalElement = matrix[0][0];
			int counterTrueMinimal = 0;

			for (int i = 0; i < heightMatrix; i++) {
				for (int j = 0; j < widthMatrix; j++) {
					if (matrix[i][j] < minimalElement) {
						if (flagTrueMinimalism == false) {
							flagTrueMinimalism = true;
							counterTrueMinimal--;
						}
						indexeMinimalI = i;
						indexeMinimalJ = j;
						minimalElement = matrix[i][j];
					} else if (matrix[i][j] == minimalElement) {
						flagTrueMinimalism = false;
						counterTrueMinimal++;
					}
				}
			}

			if (flagTrueMinimalism == true) {
				System.out.println(indexeMinimalI + " " + indexeMinimalJ);
			} else if (flagTrueMinimalism == false && counterTrueMinimal == 1) {
				System.out.println(indexeMinimalI + " " + indexeMinimalJ);
			}
		}

		scanner.close();
	}
}

// 3 4
//3 5 6 7
//8 9 39 2
//1 9 3 1

//3 4
//1 5 6 7
//8 9 39 2
//5 9 3 4

//3 3
//1 5 6 
//8 1 39 
//5 9 3 