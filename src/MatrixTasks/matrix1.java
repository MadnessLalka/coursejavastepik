package Matrix;
import java.util.Scanner;

public class matrix1 {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        int numberRowsAndColumns = scanner.nextInt();

        if (numberRowsAndColumns < 11) {
            int[][] matrix = new int[numberRowsAndColumns][numberRowsAndColumns];
            int sumOfRows = 0;
            int sumOfColumns = 0;
            int sumOfDiagonails = 0;
            int sumOfNegativDiagonails = 0;
            int jIndexCounter = 0;

            int magikMark = (numberRowsAndColumns * (numberRowsAndColumns * numberRowsAndColumns + 1)) / 2;

            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    matrix[i][j] = scanner.nextInt();
                }
            }

            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    sumOfRows += matrix[i][j];
                }
                sumOfDiagonails += matrix[i][i];
                sumOfNegativDiagonails += matrix[i][matrix.length - i - 1];
            }

            while (jIndexCounter < numberRowsAndColumns) {
                for (int i = 0; i < matrix.length; i++) {
                    sumOfColumns += matrix[i][jIndexCounter];
                }
                jIndexCounter++;
            }

            // System.out.println(sumOfRows / numberRowsAndColumns + " " + sumOfColumns /
            // numberRowsAndColumns + " "
            // + sumOfDiagonails + " " + sumOfNegativDiagonails + " " + magikMark);

            if ((sumOfRows / numberRowsAndColumns) == magikMark && (sumOfColumns / numberRowsAndColumns) == magikMark
                    && sumOfDiagonails == magikMark && sumOfNegativDiagonails == magikMark) {
                System.out.println("YES");
            } else if (sumOfRows / numberRowsAndColumns == sumOfColumns / numberRowsAndColumns
                    && sumOfDiagonails == sumOfNegativDiagonails) {
                System.out.println("YES");
            } else {
                System.out.println("NO");

            }

        }
    }
}