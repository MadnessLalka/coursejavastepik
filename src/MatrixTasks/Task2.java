package MatrixTasks;

import java.util.Scanner;

public class Task2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int heightMatrix = scanner.nextInt();
		int widthMatrix = scanner.nextInt();

		int[][] matrix = new int[heightMatrix][widthMatrix];
//		matrix[0][0] = 1;

		for (int i = 0; i < heightMatrix; i++) {
			for (int j = 0; j < widthMatrix; j++) {
				matrix[i][j] = (i + 1) * (j + 1);
			}
		}

		for (int i = 0; i < heightMatrix; i++) {
			for (int j = 0; j < widthMatrix; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println("");
		}

		scanner.close();
	}
}