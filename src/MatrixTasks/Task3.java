package MatrixTasks;

import java.util.Iterator;
import java.util.Scanner;

public class Task3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int heightMatrix = scanner.nextInt();
		int widthMatrix = scanner.nextInt();
		int[][] matrix = new int[heightMatrix][widthMatrix];

		for (int i = 0; i < heightMatrix; i++) {
			for (int j = 0; j < widthMatrix; j++) {
				matrix[i][j] = scanner.nextInt();
			}
		}
		for (int i = 0; i < heightMatrix; i++) {
			for (int j = 0; j < widthMatrix; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}

		scanner.close();
	}
}