package MatrixTasks;

import java.util.Scanner;

public class Task9 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int heightMatrix = scanner.nextInt();
		int widthMatrix = scanner.nextInt();
		if (heightMatrix != 0 || widthMatrix != 0) {

			int[][] matrix = new int[heightMatrix][widthMatrix];
			int[][] rotatedMatrix = new int[widthMatrix][heightMatrix];

			for (int i = 0; i < heightMatrix; i++) {
				for (int j = 0; j < widthMatrix; j++) {
					matrix[i][j] = scanner.nextInt();
					System.out.print(matrix[i][j] + " ");
				}
				System.out.println();
			}

			System.out.println();

			for (int i = 0; i < widthMatrix; i++) {
				for (int j = 0; j < heightMatrix; j++) {
					if (heightMatrix < widthMatrix) {
						rotatedMatrix[i][j] = matrix[j][(heightMatrix) - i];
					} else if ( heightMatrix == widthMatrix) {
						rotatedMatrix[i][j] = matrix[(heightMatrix - 1) - j][i];
					} else if (heightMatrix > widthMatrix ) {
						rotatedMatrix[i][j] = matrix[j][(widthMatrix - 1) - i];
					}
					System.out.print(rotatedMatrix[i][j] + " ");
				}
				System.out.println();
			}
			scanner.close();
		} else {
			System.out.println(0);
		}
	}
}
// 3 4 1 2 3 8 4 6 7 8 -5 6 3 4
//4 3 1 2 3 4 6 7 -5 6 3 2 6 0
// 3 3 1 2 3 4 6 7 -5 6 3

//4 3 8 8 4 3 7 3 2 6 6 1 4 -5
// 3 3 -5 4 1 6 6 2 3 7 3
// 3 4 2 -5 4 1 6 6 6 2 0 3 7 3
// 4 3 8 8 4 3 7 3 2 6 6 1 4 -5
