package Matrix;

import java.util.Scanner;

public class matrix11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int lenghtOfRows = scanner.nextInt();
        int lenghtOfColumns = scanner.nextInt();

        int[][] matrixOfHammerThrowning = new int[lenghtOfRows][lenghtOfColumns];
        int maxElement = matrixOfHammerThrowning[0][0];

        for (int i = 0; i < lenghtOfRows; i++) {
            for (int j = 0; j < lenghtOfColumns; j++) {
                matrixOfHammerThrowning[i][j] = scanner.nextInt();
                if (maxElement < matrixOfHammerThrowning[i][j]) {
                    maxElement = matrixOfHammerThrowning[i][j];
                }
            }
        }

        int counterMaxNumber = 0;
        String columnsWithRightResult = "";

        for (int i = 0; i < lenghtOfRows; i++) {
            for (int j = 0; j < lenghtOfColumns; j++) {
                if (maxElement == matrixOfHammerThrowning[i][j]) {
                    counterMaxNumber++;
                    columnsWithRightResult += i + " ";
                    break;
                }
            }
        }

        System.out.println(counterMaxNumber);
        System.out.print(columnsWithRightResult);

    }
}