package Matrix;

import java.util.Scanner;

public class matrix8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int lenghtOfArray = scanner.nextInt();

        if (lenghtOfArray < 100) {
            int[][] matrix = new int[lenghtOfArray][lenghtOfArray];

            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    matrix[i][j] = scanner.nextInt();
                }
            }

            int sumOfNumberMoreMainDiagonail = 0;
            int sumOfNumberLessMainDiagonail = 0;

            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    if (i != j && j > i) {
                        sumOfNumberMoreMainDiagonail += matrix[i][j];
                    } else if (i != j && j < i) {
                        sumOfNumberLessMainDiagonail += matrix[i][j];
                    }
                }
            }

            if (sumOfNumberLessMainDiagonail == sumOfNumberMoreMainDiagonail) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }

        }
    }
}