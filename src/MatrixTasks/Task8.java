package MatrixTasks;

import java.util.Scanner;

public class Task8 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int heightMatrix = scanner.nextInt();
		int widthMatrix = scanner.nextInt();
		
		int[][] matrix = new int[heightMatrix][widthMatrix];
		int[][] rotatedMatrix = new int[widthMatrix][heightMatrix];
		
		for (int i = 0; i < heightMatrix; i++) {
			for (int j = 0; j < widthMatrix; j++) {
				matrix[i][j] = scanner.nextInt();
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
		
		System.out.println();
		
		for (int i = 0; i < widthMatrix; i++) {
			for (int j = 0; j < heightMatrix; j++) {
				
				rotatedMatrix[i][j] = matrix[(heightMatrix - 1) - j][i];

				System.out.print(rotatedMatrix[i][j] + " ");
			}
			System.out.println();
		}
		
		scanner.close();
	}
}
// 3 4 1 2 3 8 4 6 7 8 -5 6 3 4
