package MatrixTasks;

import java.util.Scanner;

public class Task4 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int matrixLenght = scanner.nextInt();
		int[][] matrix = new int[matrixLenght][matrixLenght];

		for (int i = 0; i < matrixLenght; i++) {
			for (int j = 0; j < matrixLenght; j++) {
				matrix[i][j] = 0;
				if (i == j) {
					matrix[i][j] = 1;
				} else if (i > j) {
					matrix[i][j] = 2;
				}
			}
		}

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}

		scanner.close();
	}
}