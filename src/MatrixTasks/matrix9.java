package Matrix;

import java.util.Scanner;

public class matrix9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int lenghtOfArray = scanner.nextInt();

        if (lenghtOfArray < 11) {
            int[][] matrix = new int[lenghtOfArray][lenghtOfArray];

            for (int i = 0; i < lenghtOfArray; i++) {
                for (int j = 0; j < lenghtOfArray; j++) {
                    matrix[i][j] = scanner.nextInt();
                }
            }

            int sumOfRows = 0;
            int sumOfColumns = 0;
            int sumOfDiagonails = 0;
            int sumOfNegativDiagonails = 0;

            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    sumOfRows += matrix[i][j];
                }
                sumOfColumns += matrix[i][0];
                sumOfDiagonails += matrix[i][i];
                sumOfNegativDiagonails += matrix[i][matrix.length - i - 1];
            }

            if ((sumOfRows / lenghtOfArray) == sumOfColumns && sumOfColumns == sumOfDiagonails
                    && sumOfDiagonails == sumOfNegativDiagonails) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
    }
}