package Matrix;

import java.util.Scanner;

public class matrix5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countOfArray = scanner.nextInt();
        int[] array = new int[countOfArray];

        if (countOfArray >= 3 && countOfArray <= 100) {
            for (int i = 0; i < array.length; i++) {
                array[i] = scanner.nextInt();
            }

            int countsOfelements = 0;

            for (int i = 1; i < array.length - 1; i++) {
                if ((array[i] > array[i - 1] && array[i] > array[i + 1])) {
                    countsOfelements++;
                }

            }

            System.out.println(countsOfelements);

        }
    }
}