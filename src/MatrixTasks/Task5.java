package MatrixTasks;

import java.util.Scanner;

public class Task5 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int matrixLenght = scanner.nextInt();
		int[][] matrix = new int[matrixLenght][matrixLenght];
		int sumFirstDiagonal = 0;
		int sumSecondDiagonal = 0;

		if (matrixLenght < 100) {
			for (int i = 0; i < matrixLenght; i++) {
				for (int j = 0; j < matrixLenght; j++) {
					matrix[i][j] = scanner.nextInt();
				}
			}

			for (int i = 0; i < matrixLenght; i++) {
				for (int j = 0; j < matrixLenght; j++) {
					if (i > j) {
						sumFirstDiagonal += matrix[i][j];
					} else if (i < j) {
						sumSecondDiagonal += matrix[i][j];
					}
				}
			}

			if (sumFirstDiagonal == sumSecondDiagonal) {
				System.out.println("YES");
			} else {
				System.out.println("NO");
			}
		}

		scanner.close();
	}
}