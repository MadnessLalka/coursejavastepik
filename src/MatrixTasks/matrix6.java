package Matrix;

import java.util.Scanner;

public class matrix6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countOfArray = scanner.nextInt();
        int[] array = new int[countOfArray];

        if (countOfArray > 1 && countOfArray <= 100) {
            for (int i = 0; i < array.length; i++) {
                array[i] = scanner.nextInt();

            }
            
            int currentNumber = 0;

            boolean isSorted = false;

            while (!isSorted) {

                boolean isLessToMore = false;

                for (int i = 0; i < array.length - 1; i++) {
                    if (array[i] > array[i + 1]) {
                        currentNumber = array[i + 1];
                        array[i + 1] = array[i];
                        array[i] = currentNumber;
                    }

                }

                for (int i = 0; i < array.length - 1; i++) {
                    if (array[i] <= array[i + 1]) {
                        isLessToMore = true;
                    } else {
                        isLessToMore = false;
                        break;
                    }
                }

                if (isLessToMore) {
                    isSorted = true;
                }
            }

            for (int i = 0; i < array.length; i++) {
                System.out.print(array[i] + " ");
            }

        } else if (countOfArray == 1) {
            array[0] = scanner.nextInt();
            System.out.println(array[0]);
        }
    }
}