package DebuggingSoftwareTasks;

import java.util.Scanner;

public class Task3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int number = scanner.nextInt();
		int count = 1;
		int countNumber = 1;

		while (number > count) {
			if (number % count == 0) {
				countNumber++;
			}
			count++;
		}
		System.out.println(countNumber);

		scanner.close();
	}
}