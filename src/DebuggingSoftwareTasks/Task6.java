package DebuggingSoftwareTasks;

import java.util.Scanner;

class Task6 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int countOfChildrens = scanner.nextInt();

		if (countOfChildrens >= 2) {
			
			String childName = scanner.next();
			int childHeight = scanner.nextInt();
			int maximumHeight = childHeight;
			String maximimHeightName = "";
			int minimumHeight = childHeight;
			String minimumHeightName = "";
			Boolean flagEqualName = false;

			for (int counter = 1; counter < countOfChildrens; counter++) {
				String lastChildName = childName;
				childName = scanner.next();
				childHeight = scanner.nextInt();

				if (!lastChildName.equals(childName)) {
					if (childHeight > maximumHeight) {
						maximumHeight = childHeight;
						maximimHeightName = childName;
					} else if (childHeight == maximumHeight) {
						continue;
					}

					if (childHeight < minimumHeight) {
						minimumHeight = childHeight;
						minimumHeightName = childName;
					} else if (childHeight == minimumHeight) {
						continue;
					}

				} else {
					flagEqualName = true;
					break;
				}
			}

			if (flagEqualName == false) {
				System.out.println(minimumHeightName);
				System.out.println(maximimHeightName);
			}

		} else if (countOfChildrens == 1) {
			String childName = scanner.next();
			int childHeight = scanner.nextInt();
			System.out.println(childName);
			System.out.println(childName);
		}
		scanner.close();
	}
}