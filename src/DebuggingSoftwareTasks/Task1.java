package DebuggingSoftwareTasks;

import java.util.Scanner;

public class Task1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int counter = 0;
		boolean pin_code_coincided = true;

		while (counter != 5) {
			String correct = scanner.nextLine();
			String vasya = scanner.nextLine();

			counter++;

			if (correct.equals(vasya)) {
				System.out.println("CORRECT");
				break;
			}

			System.out.println("INCORRECT " + counter);
		}

		if (counter == 5) {
			pin_code_coincided = false;
		}

		if (!pin_code_coincided) {
			System.out.println("Error");
		}

		scanner.close();
	}
}