package DebuggingSoftwareTasks;

import java.util.Scanner;

class Task4 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int counterNumbers = 0;
		boolean flagTrueEqual = false;

		for (int number = scanner.nextInt(); number != 0; number = scanner.nextInt()) {
			if (number % 3 == 0) {
				flagTrueEqual = true;
				counterNumbers++;
			}

			if (number < 0) {
				flagTrueEqual = true;
				counterNumbers--;
			}
		}

		if (counterNumbers > 0) {
			System.out.println("333");
		} else if (counterNumbers == 0) {
			System.out.println("Equal");
		} else if (counterNumbers < 0) {
			System.out.println("negative");
		}

		scanner.close();
	}
}