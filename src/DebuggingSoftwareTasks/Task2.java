package DebuggingSoftwareTasks;

import java.util.Scanner;

public class Task2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int ageYoungerBrother = scanner.nextInt();
		int ageElderBrother = scanner.nextInt();
		Boolean flag = false;

		if (ageYoungerBrother > ageElderBrother || ageYoungerBrother == ageElderBrother) {
			System.out.println("Никогда");
		} else if (ageYoungerBrother < ageElderBrother) {
			if (ageElderBrother - ageYoungerBrother > 1) {

				int counter = 0;

				while (ageElderBrother % ageYoungerBrother != 0 || ageElderBrother / ageYoungerBrother != 2) {
					counter++;
					ageElderBrother++;
					ageYoungerBrother++;
					if (counter > 100) {
						flag = true;
						break;
					}
				}

				if (flag == true) {
					System.out.println("Никогда");
				} else if (flag == false) {
					System.out.println(2020 + counter);
				}

			} else {
				System.out.println("Никогда");
			}
		}

		scanner.close();
	}
}