package String;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String ipAdress = sc.nextLine();
        String[] ipAdressArray = ipAdress.split("\\.");
        boolean isTrueIpAdress = true;

        if (ipAdress.length() <= 15 && ipAdressArray.length == 4) {

            for (String subnet : ipAdressArray) {
                if (subnet.isEmpty()) {
                    break;
                } else {
                    if (Integer.parseInt(subnet) >= 0 && Integer.parseInt(subnet) <= 255) {
                        continue;
                        // System.out.println(Integer.parseInt(subnet));
                    } else {
                        isTrueIpAdress = false;
                        break;
                    }
                }
            }

            if (isTrueIpAdress == true) {
                System.out.println("YES");
            } else if (!isTrueIpAdress) {
                System.out.println("NO");
            }

        } else {
            System.out.println("NO");
        }

    }

}