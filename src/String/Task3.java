package String;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String replaceWord = sc.nextLine();
        String newWord = sc.nextLine();
        String recipe = sc.nextLine();

        recipe = recipe.replace(replaceWord, newWord);

        System.out.println(recipe.toLowerCase());

    }
}