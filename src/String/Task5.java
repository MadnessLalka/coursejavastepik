package String;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String newString = new String();
        newString = sc.nextLine();

        char[] arrayNumbers = (/* newString.replace(" ", "") */newString).toCharArray();

        String number = sc.next();

        int firstIndex;
        int lastIndex;

        firstIndex = (new String(arrayNumbers)).indexOf(number);
        lastIndex = (new String(arrayNumbers)).lastIndexOf(number);

        System.out.println(firstIndex);
        System.out.println(lastIndex);

    }
}