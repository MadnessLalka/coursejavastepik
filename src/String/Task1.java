package String;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        char symbol = scanner.next().charAt(0);

        if (symbol >= '0' && symbol <= '9') {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }

    }
}
