package String;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int countWords = sc.nextInt();
        String[] arrayWords = new String[countWords];

        for (int i = 0; i < arrayWords.length; i++) {
            arrayWords[i] = sc.next();
        }
        String delimiter = sc.next();

        String str = String.join(delimiter, arrayWords);
        System.out.println(str);

    }
}