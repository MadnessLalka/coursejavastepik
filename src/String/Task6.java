package String;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String mashaFavouriteLetter = sc.nextLine();
        String olegFavouriteLetter = sc.nextLine();
        String text = sc.nextLine();
        String[] mashaOlegWords = text.toLowerCase().split(" ");

        int countMashaWords = 0;
        int countOlegWords = 0;

        for (String word : mashaOlegWords) {

            char firstChar = word.charAt(0);
            char lastChar = word.charAt(word.length() - 1);

            if (String.valueOf(firstChar).equals(mashaFavouriteLetter)
                    && String.valueOf(lastChar).equals(olegFavouriteLetter)) {
                countMashaWords++;
            } else if (String.valueOf(firstChar).equals(olegFavouriteLetter)
                    && String.valueOf(lastChar).equals(mashaFavouriteLetter)) {
                countOlegWords++;
            }

        }
        System.out.print(countMashaWords + "\n" + countOlegWords);

    }
}