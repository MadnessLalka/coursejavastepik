package TernaryOperator;

import java.util.Scanner;

public class Task4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int X = sc.nextInt();
		int Y = sc.nextInt();
		int Z = sc.nextInt();
		
		System.out.println((X + Y > Z) && (Y + Z > X) && ( X + Z > Y) ? "Yes" : "No" );
		
		sc.close();
	}

}
