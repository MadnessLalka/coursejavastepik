package TernaryOperator;

import java.util.Scanner;

public class Task7 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int X1 = sc.nextInt();
		int Y1 = sc.nextInt();
		int X2 = sc.nextInt();
		int Y2 = sc.nextInt();

		if ( (X1  < 1  || X1 > 8) || (X2  < 1  || X2 > 8) || (Y1  < 1  || Y1 > 8) || (Y2  < 1  || Y2 > 8) ) {
			System.out.println("NO");
		} else {
			System.out.println(( (X1 != X2 &&  Y1 != Y2)  && (Math.abs(X1 - X2) == Math.abs(Y1 - Y2) )) ? "YES" : "NO" );	
		}
	
		sc.close();
	}
}