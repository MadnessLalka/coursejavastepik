package TernaryOperator;

import java.util.Scanner;

public class Task12 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int firstNumber = sc.nextInt();
		int secondNumber = sc.nextInt();
		int thirdNumber = sc.nextInt();
		
		if (firstNumber < secondNumber && firstNumber < thirdNumber) {
			System.out.print(firstNumber);
			if (secondNumber < thirdNumber) {
				System.out.print(" " + secondNumber + " " + thirdNumber);
			} else {
				System.out.print(" " + thirdNumber + " " + secondNumber);
			}
		} else if (secondNumber < firstNumber && secondNumber < thirdNumber) {
			System.out.print(secondNumber);
			if (firstNumber < thirdNumber) {
				System.out.print(" " + firstNumber + " " + thirdNumber);
			} else {
				System.out.print(" " + thirdNumber + " " + firstNumber);
			}
		} else if (thirdNumber < firstNumber && thirdNumber < secondNumber) {
			System.out.print(thirdNumber);
			if (firstNumber < secondNumber) {
				System.out.print(" " + firstNumber + " " + secondNumber);
			} else {
				System.out.print(" " + secondNumber + " " + firstNumber);
			}
		}
		
		sc.close();
	}
}