package TernaryOperator;

import java.util.Scanner;

public class Task1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int age = sc.nextInt();
		
		if (age >= 21) {
			System.out.println("Да");
		} else {
			System.out.println("Нет");
		}
		
		sc.close();
	}

}
