package TernaryOperator;

import java.util.Scanner;

public class Task5 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int X1 = sc.nextInt();
		int Y1 = sc.nextInt();
		int X2 = sc.nextInt();
		int Y2 = sc.nextInt();
		
		System.out.println(( (X1 > 0 && X2 > 0) && (Y1 > 0 && Y2 > 0)  || ( X1 < 0 && X2 < 0 &&  Y1 < 0 && Y2 < 0 ) || ( X1 < 0 && X2 < 0 && Y1 > 0 && Y2 > 0) || (X1 > 0 && X2 > 0 && Y1 < 0 && Y2 < 0 )) ? "YES" : "NO" );
		
		sc.close();
	}

}
