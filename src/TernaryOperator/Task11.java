package TernaryOperator;

import java.util.Scanner;

public class Task11 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int cookInOneFryingPan = sc.nextInt();
		int timeToReady = sc.nextInt();
		int  countMeatCutlets = sc.nextInt();		
		int timeConst = 32000;	
		
		 if (cookInOneFryingPan != 0 && countMeatCutlets != 0 && timeToReady != 0 &&  cookInOneFryingPan <= timeConst && countMeatCutlets <= timeConst && timeToReady <= timeConst) {
			 if ((countMeatCutlets <= cookInOneFryingPan) ) {
					System.out.println(timeToReady * 2);
			 } else if (countMeatCutlets >= 0 && cookInOneFryingPan >= 0 && timeToReady >= 0) {
				 if ( countMeatCutlets % cookInOneFryingPan != 0 ) {
					int count = countMeatCutlets / cookInOneFryingPan;
					if (countMeatCutlets % cookInOneFryingPan <= cookInOneFryingPan / 2){
						System.out.println(count * 2 * timeToReady + timeToReady);
					}  else {
						System.out.println(count * 2 * timeToReady  + timeToReady * 2 );
					}
				} else if (countMeatCutlets % cookInOneFryingPan == 0) {
					int count = countMeatCutlets / cookInOneFryingPan;
					System.out.println(count * 2 * timeToReady);
				}
			 } 
		sc.close();
	}
	}
}