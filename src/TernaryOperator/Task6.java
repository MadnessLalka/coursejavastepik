package TernaryOperator;

import java.util.Scanner;

public class Task6 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int X1 = sc.nextInt();
		int Y1 = sc.nextInt();
		int X2 = sc.nextInt();
		int Y2 = sc.nextInt();
		
		System.out.println(( (X1 == X2) || (Y1  ==  Y2) ) ? "YES" : "NO" );
		
		sc.close();
	}

}
