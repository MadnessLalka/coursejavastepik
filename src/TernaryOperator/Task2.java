package TernaryOperator;

import java.util.Scanner;

public class Task2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int age = sc.nextInt();
		
		System.out.println((age >= 21) ? "Да" : "Нет");
		
		sc.close();
	}

}
