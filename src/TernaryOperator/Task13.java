package TernaryOperator;

import java.util.Scanner;

public class Task13 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		//1 - I, 5 - V, 10 - X, 50 - L, 100 - C
		
		int numberInDecimalSystem = sc.nextInt();
		
		if (numberInDecimalSystem >= 1 && numberInDecimalSystem <= 100) {
			int bigNumber = (numberInDecimalSystem / 10) * 10;
			int smallNumber = (numberInDecimalSystem % 10);
			
			if (bigNumber == 10) {
				System.out.print("X");
			} else if (bigNumber == 20) {
				System.out.print("XX");
			} else if (bigNumber == 30) {
				System.out.print("LV");
			}  else if (bigNumber == 40) {
				System.out.print("XL");
			} else if (bigNumber == 50) {
				System.out.print("L");
			} else if (bigNumber == 60) {
				System.out.print("LX");
			} else if (bigNumber == 70) {
				System.out.print("LXX");
			} else if (bigNumber == 80) {
				System.out.print("LXXX");
			} else if (bigNumber == 90) {
				System.out.print("XC");
			} else if (bigNumber == 100) {
				System.out.print("C");
			}
			
			if (smallNumber == 1) {
				System.out.print("I");
			} else if (smallNumber == 2) {
				System.out.print("II");
			} else if (smallNumber == 3) {
				System.out.print("III");
			}  else if (smallNumber == 4) {
				System.out.print("IV");
			} else if (smallNumber == 5) {
				System.out.print("V");
			} else if (smallNumber == 6) {
				System.out.print("VI");
			} else if (smallNumber == 7) {
				System.out.print("VII");
			} else if (smallNumber == 8) {
				System.out.print("VIII");
			} else if (smallNumber == 9) {
				System.out.print("IX");
			} 
	
		
			sc.close();
		}
	}
}