package TernaryOperator;

import java.util.Scanner;

public class Task10 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int number = sc.nextInt();
		String wordCow = "коров";
		
		if (number < 100) {
			 if (number == 11 || number == 12 || number == 13 || number == 14) {
				System.out.println(number + " " + wordCow);
			} else if (number == 1 ||  ( number % 10 == 1) ) {
				System.out.println(number + " " + wordCow + "а");
			} else if (number == 2 || number == 3 || number == 4 || (number % 10 == 2 || number % 10 == 3 || number % 10 == 4) ) {
				System.out.println(number + " " + wordCow + "ы");
			}  else {
				System.out.println(number + " " + wordCow);
			}
		}
		
		sc.close();
	}
}