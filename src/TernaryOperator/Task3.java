package TernaryOperator;

import java.util.Scanner;

public class Task3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int X = sc.nextInt();
		int Y = sc.nextInt();
		
		System.out.println(Math.sqrt((Math.pow(X, 2) + Math.pow(Y, 2))));
		
		sc.close();
	}

}
